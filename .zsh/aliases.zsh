
#Navigation and Basics
alias lns="ln -s"
alias lsa="ls -a --color=auto"
alias lsl="ls -l --color=auto"
alias ls="ls --color=auto"
alias sudo="sudo "
alias subal="subl ~/.zsh/aliases.zsh"
alias rmatal="rmate ~/.zsh/aliases.zsh"

#apt-get aliases

alias agi="sudo apt-get install"
alias agu="sudo apt-get update"
alias agg="sudo apt-get update && sudo apt-get upgrade"
alias agr="sudo	apt-get remove"
alias acs="sudo apt-cache search"


# SSH aliases
alias sshr="ssh -R 52698:localhost:52698 "


# Aliases leveraging the cb() and cbf() functions
# These are custom defined shell functions
# ------------------------------------------------

# Copy SSH public key
#alias cbssh="cbf ~/.ssh/id_rsa.pub" 
# Copy current working directory
alias cbwd="pwd | cb"  
# Copy most recent command in bash history
alias cbhs="cat $HISTFILE | tail -n 1 | cb"


# Git
alias gp='git push origin HEAD'
alias gd='git diff'
alias gc='git commit'
alias gaa='git add -A'
alias gca='git commit -a'
alias gcma='git commit -a -m'
alias gcm='git commit -m'
alias gb='git branch'
alias gs='git status -sb' 

# ejabberd

alias ectl="sudo ejabberdctl"


### ### ### ### ### ### 
# Docker
### ### ### ### ### ### 

# Basics
alias docker='sudo docker'			#because this isn't a bad idea, you lazy bastard
alias dcreate='sudo docker create'	#creates a container but does not start it.
alias drun='sudo docker run'		#creates and starts a container in one operation.
alias drit='sudo docker run -t -i'	#creates, starts and opens an interactive terminal in a container
alias dstart='sudo docker start'	#starts a container.
alias drestart='sudo docker restart'	#restarts a container, to be used on running containers
alias dattach='sudo docker attach'	#will connect to a running container.
alias dwait='sudo docker wait'		#blocks until container stops.
alias dstop='sudo docker stop'		#stops a container
alias dkill='sudo docker kill'		#sends a SIGKILL to a container
alias drm='sudo docker rm'			#deletes a container.
alias drmv='sudo docker --rm -v'	#deletes container AND associated volume(s)

# Docker Info commands
alias dims='sudo docker images'		#shows all images.
alias dps='sudo docker ps'			#shows running containers.
alias dpsa='sudo docker ps -a' 		#shows running and stopped containers.
alias dlogs='sudo docker logs'		#gets logs from container.
alias dspect='sudo docker inspect'	#looks at all the info on a container (including IP address).
alias devents='sudo docker events'	#gets events from container.
alias dport='sudo docker port'		#shows public facing port of container.
alias dtop='sudo docker top'		#shows running processes in container.
alias dstat='sudo docker stats'		#shows containers' resource usage statistics.
alias dstats='sudo docker stats'	#same as above
alias ddiff='sudo docker diff'		#shows changed files in the container's FS.
alias dhist='sudo docker history'	#show image history
alias dtag=	'sudo docker tag'		#tags an image to a name (local or reg)


# Docker interactive commands
alias dcp='sudo docker cp'			#copies files or folders out of a container's filesystem.
alias dexp='sudo docker export'		#turns container filesystem into tarball archive stream to STDOUT.
alias dexec='sudo docker exec'		#to execute a command in container.
alias dimp='sudo docker import'		#creates an image from a tarball.
alias dbld='sudo docker build'		#creates image from Dockerfile.
alias dcm='sudo docker commit'		#creates an image from a container
alias drmi='sudo docker rmi'		#removes an image
alias dload='sudo docker load'		#loads an image from a tar archive as STDIN, including iamges and tags
alias dsave='sudo docker save'		#saves an image to a tar archive to STDOUT with all parent layers and tags

# Docker Registry and Repo
alias dlogin='sudo docker login'	#login to a registry
alias dsearch='sudo docker search'	#searches a registry for an image
alias dull='sudo docker pull'		#pulls an image from registry to local machine
alias dush='sudo docker push'		#pushes an image to a registry from local
