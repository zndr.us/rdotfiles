# HISTORY
HISTSIZE=10000
SAVEHIST=9000
HISTFILE=~/.zsh/.zsh_history

#append history list to the history file, rather than replace it. 
#Thus, multiple parallel zsh sessions will all have the new entries 
#from their history lists added to the history file, 
#in the order that they exit
setopt APPEND_HISTORY

#Save each command’s beginning timestamp (in seconds since the epoch)
#and the duration (in seconds) to the history file
setopt EXTENDED_HISTORY 
