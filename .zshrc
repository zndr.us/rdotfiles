#importing modules

source ~/.zsh/aliases.zsh
source ~/.zsh/colors.zsh
source ~/.zsh/prompt.zsh
source ~/.zsh/completion.zsh
source ~/.zsh/functions.zsh
source ~/.zsh/history.zsh


# ===== Basics
setopt no_beep # don't beep on error
#setopt interactive_comments # Allow comments even in interactive shells (especially for Muness)

# ===== Changing Directories
setopt auto_cd # If you type foo, and it isn't a command, and it is a directory in your cdpath, go there
setopt cdablevarS # if argument to cd is the name of a parameter whose value is a valid directory, it will become the current directory
setopt pushd_ignore_dups # don't push multiple copies of the same directory onto the directory stack


# ===== Scripts and Functions
setopt multios # perform implicit tees or cats when multiple redirections are attempted







